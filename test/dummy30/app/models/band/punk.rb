class Band::Punk < Band
  validates_presence_of :name
  followable_behaviour
end
