class Band::Punk::PopPunk < Band::Punk
  validates_presence_of :name
  followable_behaviour
end
