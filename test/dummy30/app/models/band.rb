class Band < ApplicationRecord
  validates_presence_of :name
  followable_behaviour
end
