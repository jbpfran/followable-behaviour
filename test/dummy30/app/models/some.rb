class Some < CustomRecord
  validates_presence_of :name
  follower_behaviour
  followable_behaviour
end
